const express = require('express')
const cors = require('cors')
const bodyParser = require('body-parser')
const admin = require('firebase-admin')
const config = require('./config')
const router = require('./routes/jenosize')

let app = express()

app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: false }))

admin.initializeApp({
    credential: admin.credential.cert(config.firebase.serviceAccountKeyPath),
    databaseURL: config.firebase.databaseURL
})

app.use('/', router)

const port = process.env.PORT || 8080
app.listen(port, () => {
    console.log(`Listening on ${port}!`)
})
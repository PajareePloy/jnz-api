const config = {
    googlePlaces: {
        apiKey: process.env.GOOGLE_API_KEY || 'AIzaSyAPSn3BHB3sKSs3bw8aPD4mK3ir7hUPQbk'
    },
    firebase: {
        serviceAccountKeyPath: process.env.GOOGLE_CONFIG_BASE64 ? JSON.parse(Buffer.from(process.env.GOOGLE_CONFIG_BASE64, 'base64').toString('ascii')) : '/Users/Pajaree/JNZ/jnz-api/serviceAccountKey.json',
        databaseURL: process.env.DATABASE_URL || 'https://jenosize-1454d.firebaseio.com'
    }
}

module.exports = config
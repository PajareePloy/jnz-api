const axios = require('axios')
const admin = require('firebase-admin')

const config = require('../config')
const { getGame24Result, checkWin, findBestPosition } = require('../services/jenosize')

const searchRestaurant = async (req, res) => {
    try {
        const searchText = req.params.searchText
        const type = 'restaurant'
        const key = config.googlePlaces.apiKey
        const url = `https://maps.googleapis.com/maps/api/place/textsearch/json?query=${searchText}&type=${type}&key=${key}`
        const response = await axios.get(url)
        res.json(response.data.results)
    }
    catch (err) {
        console.log(err)
        res.status(500).json({
            error: 'Find Restaurant from Google Places Failed.'
        })
    }
}

const game24 = (req, res) => {
    try {
        const { num1, num2, num3, num4 } = req.params
        if (num1 < 1 || num1 > 9 || num2 < 1 || num2 > 9 || num3 < 1 || num3 > 9 || num4 < 1 || num4 > 9) {
            res.send('The numbers must be between 1 and 9.')
        }
        const result = getGame24Result(req.params)
        res.send(result)
    }
    catch (err) {
        console.log(err)
        res.status(500).json({
            error: 'Game 24 Error.'
        })
    }
}

const authMiddleware = async (req, res, next) => {
    try {
        if (req.url.startsWith('/public')) {
            next()
        }
        else {
            const token = req.headers.authorization
            if (token) {
                const decodeToken = await admin.auth().verifyIdToken(token)
                if (decodeToken) {
                    req.body.uid = decodeToken.uid
                    next()
                }
                else {
                    res.status(401).json({
                        error: 'You are not authorized.'
                    })
                }
            }
            else {
                res.status(401).json({
                    error: 'You are not authorized.'
                })
            }
        }
    }
    catch (err) {
        console.log(err)
        res.status(500).json({
            error: 'Authentication Failed.'
        })
    }
}

const testMiddleWare = (req, res) => {
    res.json({ message: 'Congratulations, you have been authorized.' })
}

const signUpWithEmail = async (req, res) => {
    try {
        const { email, password, displayName } = req.params
        const data = { email, password, displayName }
        const user = await admin.auth().createUser(data)
        res.send('Sign Up Completed.')
    }
    catch (err) {
        console.log(err)
        res.status(500).json({
            error: 'Sign Up Failed. ' + err.errorInfo.message
        })
    }
}

 const ticTacToeRobotTurn = (req, res) => {
    const { board, turn } = req.body

    const robotPlayer = turn
    const userPlayer = robotPlayer === 'X' ? 'O' : 'X'
    const playedCell = board.filter(s => s === 'X' || s === 'O')

    let ans
    if (playedCell.length === 0) {
        return res.json({ index: 4 })
    }
    else if (playedCell.length === 1) {
        if (board[4] === '') {
            return res.json({ index: 4 })
        } else {
            return res.json({ index: 0 })
        }
    }
    else if (playedCell.length === 2) {
        const userAnsed = board.findIndex((s => s === userPlayer))
        if (userAnsed === 0 || userAnsed === 8) {
            ans = 6
        } 
        else if (userAnsed === 2 || userAnsed === 6 || userAnsed === 1 || userAnsed === 3) {
            ans = 0
        }
        else if (userAnsed === 5 || userAnsed === 7) {
            ans = 8
        }
        return res.json({ index: ans })
    } 
    else {
        let newBoard = []
        for (let i=0; i<board.length; i++) {
            if (board[i] === '') {
                newBoard[i] = i
            }
            else {
                newBoard[i] = board[i]
            }
        }
        const emptyCell = newBoard.filter(s => s !== 'X' && s !== 'O')
        for (let i=0; i<emptyCell.length; i++) {
            let checkBoard = [...board]
            checkBoard[emptyCell[i]] = robotPlayer
            const win = checkWin(checkBoard, robotPlayer)
            if (win === true) {
                return res.json({ index: emptyCell[i] })
            }
        }
        for (let i=0; i<emptyCell.length; i++) {
            let checkBoard = [...board]
            checkBoard[emptyCell[i]] = userPlayer
            const lose = checkWin(checkBoard, userPlayer)
            if (lose === true) {
                return res.json({ index: emptyCell[i] })
            }
        }
        const ans = findBestPosition(newBoard, turn, robotPlayer, userPlayer)
        return res.json({ index: ans.index })
    }
}

module.exports = { searchRestaurant, ticTacToeRobotTurn, signUpWithEmail, testMiddleWare, authMiddleware, game24 }
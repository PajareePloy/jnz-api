const express = require('express')
const controller = require('../controllers/jenosize')

const router = express.Router()

router.use(controller.authMiddleware)

router.get('/public/searchRestaurant/:searchText', controller.searchRestaurant)
router.get('/public/game24/:num1/:num2/:num3/:num4', controller.game24)
router.post('/public/ticTacToeRobotTurn', controller.ticTacToeRobotTurn)
router.get('/testMiddleWare', controller.testMiddleWare)

//Temporary used for prepare data
router.get('/public/signUpWithEmail/:email/:password/:displayName', controller.signUpWithEmail)

module.exports = router
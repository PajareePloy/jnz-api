 function getGame24Result(numbers) {

    const numSets = setNumber(numbers)
    const opers = ['+', '-', '*', '/']
    const equaFormat = [1, 2, 3, 4, 5]
    let resText = '<b>NO</b>'

    for (let n=0; n < numSets.length; n++) {
        for (let i=0; i < opers.length; i++) {
            for (let j=0; j < opers.length; j++) {
                for (let k=0; k < opers.length; k++) {
                    for (let e=0; e < equaFormat.length; e++) {
                        const data = {
                            nums: numSets[n],
                            opers: [opers[i], opers[j], opers[k]],
                            format: equaFormat[e]
                        }
                        const result = isResult24(data)
                        if (result.answer) {
                            resText = `<b>YES</b><br/>One of equation is ${result.equation}`
                            return resText
                        }
                    }
                }
            }
        }
    }
    return resText
}

function setNumber(numbers) {
    const { num1, num2, num3, num4 } = numbers
    const nums = [num1, num2, num3, num4]
    let numSets = []
    nums.forEach((n1, i) => {
        nums.forEach((n2, j) => {
            if (j !== i) {
                const set = [n1, n2]
                nums.forEach((n3, k) => {
                    let finalSet = [...set]
                    if (k !== i && k !== j) {
                        finalSet.push(n3)
                        nums.forEach((n4, l) => {
                            if (l !== i && l !== j && l !== k) {
                                finalSet.push(n4)
                                numSets.push(finalSet)
                            }
                        })
                    }
                })
            }
        })
    })
    return numSets
}

function isResult24(data) {
    const goal = 24
    let result = {
        answer: false,
        equation: null
    }
    const equation = getEquation(data)
    const total = eval(equation)
    if (total === goal) {
        result.answer = true
        result.equation = equation   
    }
    return result
}

function getEquation(data) {
    let equation
    if (data.format === 1) {
        equation = '(' + data.nums[0] + data.opers[0] + data.nums[1] + ')' + data.opers[1] + '(' + data.nums[2] + data.opers[2] + data.nums[3] + ')'
    }
    else if (data.format === 2) {
        equation = '(' + data.nums[0] + data.opers[0] + '(' + data.nums[1] + data.opers[1] + data.nums[2] + ')' + ')' + data.opers[2] + data.nums[3]
    }
    else if (data.format === 3) {
        equation = '(' + '(' + data.nums[0] + data.opers[0] + data.nums[1] + ')' + data.opers[1] + data.nums[2] + ')' + data.opers[2] + data.nums[3]
    }
    else if (data.format === 4) {
        equation = data.nums[0] + data.opers[0] + '(' + data.nums[1] + data.opers[1] + '(' + data.nums[2] + data.opers[2] + data.nums[3] + ')' + ')'
    }
    else if (data.format === 5) {
        equation = data.nums[0] + data.opers[0] + '(' + '(' + data.nums[1] + data.opers[1] + data.nums[2] + ')' + data.opers[2] + data.nums[3] + ')'
    }
    return equation
}

function checkWin(board, turn) {
    if ((board[0] === turn && board[1] === turn && board[2] === turn) ||
        (board[3] === turn && board[4] === turn && board[5] === turn) ||
        (board[6] === turn && board[7] === turn && board[8] === turn) ||
        (board[0] === turn && board[3] === turn && board[6] === turn) ||
        (board[1] === turn && board[4] === turn && board[7] === turn) ||
        (board[2] === turn && board[5] === turn && board[8] === turn) ||
        (board[0] === turn && board[4] === turn && board[8] === turn) ||
        (board[2] === turn && board[4] === turn && board[6] === turn)) {
            return true
    } else {
        return false
    }
}

function findBestPosition(newBoard, turn, robotPlayer, userPlayer) {
    let board = newBoard
    const emptyCell = board.filter(s => s !== 'X' && s !== 'O')

    if (checkWin(emptyCell, userPlayer)) {
        return { score: -10 }
    }
    else if (checkWin(emptyCell, turn)) {
        return { score: 10 }
    }
    else if (emptyCell.length === 0) {
        return { score: 0 }
    }
      
    let moves = []
    for (let i = 0; i < emptyCell.length; i++) {
        let move = {}
        move.index = board[emptyCell[i]]
        board[emptyCell[i]] = turn
      
        if (turn === robotPlayer) {
            const result = findBestPosition(emptyCell, userPlayer)
            move.score = result.score
        }
        else {
            const result = findBestPosition(emptyCell, robotPlayer)
            move.score = result.score
        }         
        board[emptyCell[i]] = move.index
        moves.push(move)
    }
      
    let bestPosition
    if ( turn === robotPlayer ) {
        let bestScore = -10000
        for (let i=0; i<moves.length; i++) {
            bestScore = moves[i].score
            bestPosition = i
        }
    }
    else {
        let bestScore = 10000
        for (let i=0; i<moves.length; i++) {
            if( moves[i].score < bestScore ) {
                bestScore = moves[i].score
                bestPosition = i
            }
        }
    }
    return moves[bestPosition]
}

module.exports = {findBestPosition, checkWin, getEquation, isResult24, setNumber, getGame24Result}